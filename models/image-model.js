const mongoose = require("../db");
const schema = new mongoose.Schema({
    filename: string,
    originalName: string,
    desc: string,
},
{
     strict: true,
     versionKey: false,
     timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
}
);

module.exports = mongoose.model('Images', schema)