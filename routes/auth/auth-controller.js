const express = require('express');
const router = express.Router();
const Users = require("../../models/user-model");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../../config/index');

// router.post('/login', function (req, res) {
//   console.log(req.body.email)
//   Users.findOne({ email: req.body.email }).select('+password')
//     .then((user) => {
//       if (!user)
//         return res.status(401).send('No user found.');
//       // let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
//       // if (!passwordIsValid)
//       //   return res.status(404).send('Please enter valid password.');
//       // let token = jwt.sign({ id: user.id }, config.secret, {
//       //   expiresIn: config.expiresIn
//       // });
//       user.password = '';
//       let responseUser = { auth: true, user: user };
//       return res.status(200).send(responseUser);
//     })
//     .catch((error) => {
//       console.log(error);
//       return res.status(500).send('Internal server error!.');;
//     });
// });

router.post('/login',function(req,res){
  Users.findOne({email:req.body.email}).select('+password')
  .then((user) =>
      {
          if(!user)
              return res.status(401).send('No user found.');
          let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
          console.log(req.body)
          console.log(user, passwordIsValid)
              if (!passwordIsValid)
                return res.status(404).send('Please enter valid user password.');
          let token = jwt.sign({ id: user.id }, config.secret, {
              expiresIn: config.expiresIn
      });
      user.password = '';
      req.sessions = user;
     //req.sessions.id =sessionID;
  
      let Result = { auth: true, token: token, user: req.sessions, sessionID: req.sessions.id };
      console.log(req.sessions);
      console.log(req.sessions.id);
      return res.status(200).send(Result);
})
.catch((error) => {
  console.log(error);
  return res.status(500).send('Internal server error!.');;
});
});


module.exports = router;