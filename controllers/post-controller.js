const Post = require('../models/post-model');
// Create a new post
exports.create = (req, res) => {
    userPost: new Post();
    console.log("req",req.body);  
   // return req;     
  //  console.log("req.body.user",req.body.user,  req.body.isActive, JSON.stringify(req.body.file) );
  
    if (!req.body.content || !req.body.user) {       
        return res.status(400).send({
            message: "Required field can not be empty"
        });
    }

    if(req.file){
        this.userPost = new Post({ 
            content: req.body.content,
            isActive: req.body.isActive,
            user: req.body.user,
            file: req.file.path
            });
    }else
    {
        this.userPost = new Post({ 
            content: req.body.content,
            isActive: req.body.isActive,
            user: req.body.user       
            });
    }

    // Create a Post
   
    this.userPost.save().then(data => {
        console.log("saved data", data)
        res.send(data);
    }).catch(err => {
        console.log("error", err);
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Post."
        });
    });

};

//find all posts
exports.findAll = (req, res) => {
    Post.find().populate('user').populate('like')
        .then(posts => {
            console.log("post all data", posts);
            res.status(200).send(posts);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Error Occured"
            });
        });
};

