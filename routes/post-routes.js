const express = require('express');
const router = express.Router();
const postController = require('../controllers/post-controller');
const likeController = require('../controllers/like-controller');
router.post('/', postController.create);
router.get('/', postController.findAll);
router.post('/like', likeController.create);
module.exports = router;