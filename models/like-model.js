
const mongoose = require("../db");
const schema = new mongoose.Schema({
    islike:{
        desc: "is Liked",
        type: Boolean,
        default: false,
        required: true
    },
    post: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Posts'
      },
    user: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Users'
      }
    }, 
    {
    strict: true,
    versionKey: false,
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
});

module.exports = mongoose.model('Likes', schema);
