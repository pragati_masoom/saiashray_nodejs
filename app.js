let express = require("express");
const path = require('path');
const config = require("./config");
let app = require('express')();
let server = require('http').createServer(app);
let multer = require('multer');
  
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'media_files')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  }
})
let upload = multer({ storage: storage });
const port = process.env.PORT || config.port;
const isTestSetup = config.isTestSetup;
const cors = require('cors');
app.use(cors({
  'allowedHeaders': ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'x-access-token'],
  'exposedHeaders': ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'x-access-token'],
  'origin': '*',
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
  'preflightContinue': false
}));


// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", 'http://localhost:8101');
//   res.header("Access-Control-Allow-Credentials", true);
//   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
//   res.header("Access-Control-Allow-Headers",
// 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json,Authorization');
//   next();
// });

const bodyParser = require('body-parser');
const auth = require('./routes/auth/verify-token');



if(isTestSetup) {
	require('./config/setup');
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/auth', require('./routes/auth/index'));
app.use('/users', auth, require('./routes/user-routes'));
app.use('/signup', require('./routes/register-routes'));
app.use('/post', upload.single('file'), require('./routes/post-routes'));
app.use('/media_files',express.static('media_files'));


server.listen(port, function(){
   console.log('listening in http://localhost:' + port);
});

