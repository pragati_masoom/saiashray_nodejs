module.exports = {
    mongoConfig: {
      url: 'mongodb://127.0.0.1:27017/SaiAshray'
    },
    port: 4000,
    secret: 'supersecret',
    expiresIn: 86400, // expires in 24 hours,
    isConsoleLog: true,
    isTestSetup: false	
  };