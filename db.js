const config = require("./config");
const mongoose = require("mongoose");

mongoose.Promise = Promise;

mongoose.connect(config.mongoConfig.url)
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));

module.exports = mongoose;

