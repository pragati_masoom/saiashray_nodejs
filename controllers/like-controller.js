const Like = require('../models/like-model');
const Post = require('../models/post-model');
// Create a new post
exports.create = (req, res) => {
    console.log("like req",req.body);  
   // return req;     
    if (!req.body.post || !req.body.user) {       
        return res.status(400).send({
            message: "Required field can not be empty"
        });
    }

    // Create a Like
    var postid = req.body.post;
    const like = new Like({
        post: req.body.post,
        islike: req.body.islike,
        user: req.body.user,
    });
    like.save().then(data => {
            Post.findById(postid).then(response => {
                 response.like.push(data.id);
                 response.save();                 
            })
        res.send(data);     
    }).catch(err => {
        console.log("error", err);
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Post."
        });
    });

};

//find all posts
exports.findAll = (req, res) => {
    Post.find().populate('user')
        .then(posts => {
            res.status(200).send(posts);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Error Occured"
            });
        });
};

